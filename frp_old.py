import xl
import xlrd
import string
from collections import defaultdict
import os
import sys
import math
from pandas import read_excel
from PySide.QtCore import *
from PySide.QtGui import *
import sqlite3

class FRP_MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setGeometry(400, 100, 270, 100)
        self.setWindowTitle('FRP Report Generator')

        self.centralWidget = FRP_widget()
        self.setCentralWidget(self.centralWidget)

        # background image for QMainWindow
        # palette = QPalette()
        # palette.setBrush(QPalette.Background, QBrush(QPixmap('ppsb_logo.jpg').scaled(self.size())))
        # self.setPalette(palette)

        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
        self.statusBar.showMessage('Ready')
        if settingWindowHeight and settingWindowWidth:
            self.resize(settingWindowWidth, settingWindowHeight)

    def updateStatusBar(self, message):
        self.statusBar.showMessage(message)


class FRP_widget(QWidget):
    def __init__(self, parent=None):
        super(FRP_widget, self).__init__(parent)
        self.setWindowTitle('FRP Report Generator')
        self.getSettings()
        # self.resize(320, 200)

        # mapping file
        gridLayout = QGridLayout()
        lblMap = QLabel("Mapping File")
        self.mappingFile = QLineEdit(); self.mappingFile.setReadOnly(True)
        self.btnMappingFile = QPushButton('...')

        lblInputFolder = QLabel("Input Folder")
        self.inputFolder = QLineEdit(); self.inputFolder.setReadOnly(True)
        self.btnInputFolder = QPushButton('...')

        lblOutputFolder = QLabel("Output Folder")
        self.outputFolder = QLineEdit(); self.outputFolder.setReadOnly(True)
        self.btnOutputFolder = QPushButton('...')

        gridLayout.addWidget(lblMap, 0, 0)
        gridLayout.addWidget(self.mappingFile, 0, 1)
        gridLayout.addWidget(self.btnMappingFile, 0, 2)

        gridLayout.addWidget(lblInputFolder, 1, 0)
        gridLayout.addWidget(self.inputFolder, 1, 1)
        gridLayout.addWidget(self.btnInputFolder, 1, 2)

        gridLayout.addWidget(lblOutputFolder, 2, 0)
        gridLayout.addWidget(self.outputFolder, 2, 1)
        gridLayout.addWidget(self.btnOutputFolder, 2, 2)

        '''
        # check box
        checkLayout = QVBoxLayout()
        self.chkGenerateData = QCheckBox('Generate Data', self)
        self.chkGenerateData.setCheckState(Qt.Checked)
        self.chkGenerateReport = QCheckBox('Generate Report', self)
        self.chkGenerateReport.setCheckState(Qt.Checked)
        checkLayout.addWidget(self.chkGenerateData)
        checkLayout.addWidget(self.chkGenerateReport)
        '''

        self.btnCancel = QPushButton('Cancel')
        self.btnExecute = QPushButton('Execute')
        btnLayout = QHBoxLayout()
        btnLayout.addWidget(self.btnCancel)
        btnLayout.addWidget(self.btnExecute)

        layout = QVBoxLayout()
        layout.addLayout(gridLayout)
        #layout.addLayout(checkLayout)
        layout.addLayout(btnLayout)
        self.setLayout(layout)

        self.btnMappingFile.clicked.connect(self.onBtnMappingFileClicked)
        self.btnInputFolder.clicked.connect(self.onBtnInputFolderClicked)
        self.btnOutputFolder.clicked.connect(self.onBtnOutputFolderClicked)
        #self.chkGenerateData.stateChanged.connect(self.updateUi)
        #self.chkGenerateReport.stateChanged.connect(self.updateUi)
        self.btnCancel.clicked.connect(self.exit_)
        self.btnExecute.clicked.connect(self.process)
        
        self.initUI()

    def initUI(self):
        if mappingFile:
            self.mappingFile.setText(mappingFile)
        if inputFolder:
            self.inputFolder.setText(inputFolder)
        if outputFolder:
            self.outputFolder.setText(outputFolder)

    def onBtnMappingFileClicked(self):
        global mappingFile

        filename, filter = QFileDialog.getOpenFileName(parent=self, caption='Get Mapping File', dir='.', filter='*.xls')

        if filename:
            self.mappingFile.setText(filename)
            mappingFile = filename

    def onBtnInputFolderClicked(self):
        global inputFolder

        directory = QFileDialog.getExistingDirectory(self, caption='Select Input Directory', options=QFileDialog.ShowDirsOnly)

        if directory:
            self.inputFolder.setText(directory)
            inputFolder = directory

    def onBtnOutputFolderClicked(self):
        global outputFolder

        directory = QFileDialog.getExistingDirectory(self, caption='Select Output Directory', options=QFileDialog.ShowDirsOnly)

        if directory:
            self.outputFolder.setText(directory)
            outputFolder = directory

    def process(self):
        # get data if chkGenerateData is checked
        self.truncateTables()
        self.readFrpMapping()
        #self.getDailyTrialBalance()
        self.getGLSources()
        #self.writeFrpTemp()

        self.writeFrpFinal()
        self.btnCancel.setText('Exit')
        self.btnExecute.setEnabled(False)
        QMessageBox.information(self, "FRP Report Generation", "The process has been completed.")

    def updateUi(self):
        if not self.chkGenerateData.isChecked() and not self.chkGenerateReport.isChecked():
            self.btnExecute.setEnabled(False)
        else:
            self.btnExecute.setEnabled(True)

    def exit_(self):
        if self.btnCancel.text() == 'Cancel':
            # show the confirmation message
            flags = QMessageBox.StandardButton.Yes
            flags |= QMessageBox.StandardButton.No
            question = "Do you really want to cancel?"
            response = QMessageBox.question(self, "Confirm Cancel", question, flags, QMessageBox.No)
            if response == QMessageBox.No:
                return

        self.exitProg(0)

    def showCriticalMessage(self, msg):
        flags = QMessageBox.StandardButton.Ok
        QMessageBox.critical(self, "CRITICAL ERROR", msg, flags)

    def truncateTables(self):
    	cur.execute('DELETE FROM GLSOURCE')
    	cur.execute('DELETE FROM GLDEST')
    	cur.execute('DELETE FROM GLCODES')
    	conn.commit()

    def readFrpMapping(self):
        global glCodes, glSources

        try:
            # wb = xl.Workbook(currentPath + '\\FRP Mapping.xlsx')
            converters = {column: str for column in range(8)}
            df = read_excel(mappingFile, converters=converters)
        except:
            self.showCriticalMessage("Cannot open %s file" % mappingFile)
            self.exitProg(1)

        glCodes = defaultdict(dict)
        glSources = []

        self.parent().updateStatusBar('Reading %s' % mappingFile)
        cnt = 0
        for row in df.itertuples():
            cnt = cnt + 1
            self.parent().updateStatusBar('Reading %s record # %d' % (mappingFile, cnt))
            gl = row[1]
            src_file = row[2]
            src_sheet = row[3]
            src_col = str(row[4]).strip()
            src_cell = str(row[5]).strip()
            dest_file = row[6]
            dest_sheet = row[7]
            dest_col = str(row[8]).strip()
            dest_cell = str(row[9]).strip()

            # continue if gl is blank
            if not gl:
                continue

            # save in glcodes table
            cur.execute("SELECT * FROM GLCODES WHERE GL_CODE = '{0}' and SRC_FILE = '{1}' and SRC_SHEET = '{2}' and SRC_COL = '{3}' and SRC_CELL = '{4}'\
                        and DEST_FILE = '{5}' and DEST_SHEET = '{6}' and DEST_COL = '{7}' and DEST_CELL = '{8}'".\
                        format(gl, src_file, src_sheet, src_col, src_cell, dest_file, dest_sheet, dest_col, dest_cell))
            rec = cur.fetchall()

            # if record does not exists
            if not rec:
                sql_stmt = "INSERT INTO GLCODES(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)"\
                            " VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')".format(gl, src_file, src_sheet, src_col, src_cell, 
                                    dest_file, dest_sheet, dest_col, dest_cell)
                #print sql_stmt
                cur.execute(sql_stmt)
                #conn.commit()


            '''
            # save in gldest table
            sql_stmt = "SELECT * FROM GLDEST WHERE GL_CODE = '{0}' and DEST_FILE = '{1}' and DEST_SHEET = '{2}' and DEST_COL = '{3}' and DEST_CELL = '{4}'".\
                        format(gl, dest_file, dest_sheet, dest_col, dest_cell)
            cur.execute(sql_stmt)
            rec = cur.fetchall()

            # if record does not exists
            if not rec:
                sql_stmt = "INSERT INTO GLDEST(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)"\
                            " VALUES('{0}', '{1}', '{2}', '{3}', '{4}')".format(gl, dest_file, dest_sheet, dest_col, dest_cell)
                cur.execute(sql_stmt)
                #conn.commit()

			
			# save in glcodes table (w/ amount)                
            cur.execute("SELECT * FROM GLCODES WHERE GL_CODE = '{0}'".format(gl))
            rec = cur.fetchall()
            if not rec:
            	cur.execute("INSERT INTO GLCODES(GL_CODE) VALUES('{0}')".format(gl))
                #conn.commit()
        ''' 
        conn.commit()

    def getDailyTrialBalance(self):
        global glCodes

        try:
            df = read_excel(currentPath + '\\DailyTrial_Balance.xls', converters={'GL Account Code':str})
        except:
            self.showCriticalMessage("Cannot open 'DailyTrial_Balance.xls' file")
            sys.exit(1)

        cnt = 1
        for row in df.itertuples():
            cnt = cnt + 1
            self.parent().updateStatusBar("Reading 'DailyTrial_Balance.xls' record : " + str(cnt))
            glCode = row[1]
            if type(glCode) == int:
                glCode = str(glCode)

            balance = row[8]
            if type(balance) == unicode or type(balance) == str:
                try:
                    balance = toFloat(balance)
                except:
                    balance = 0

            if glCode in glCodes:
                glCodes[glCode]['amount'] += balance

        """
        exitLoop = False
        for row in range(6, 600000):
            if exitLoop:
                break
            for col in range(1,9):
                colName = chr(ord('A') + col -1)
                cellName = "%s%d" % (colName, row)
                try:
                    data = dtb.get(cellName).get()
                except xl.range.ExcelRangeError:
                    exitLoop = True
                    break

                #print "cell : {0}\tdata : {1}".format(cellName, data)
                if colName == 'A':                # GL Code
                    if not data:
                        continue
                    else:
                        glCode = data
                elif colName =='B':
                    if not data:                # Account Name
                        continue
                    else:
                        accountName = data
                elif colName == 'H':                # Balance
                    if not data:
                        continue
                    else:
                        balance = toFloat(data)
                        if glCode in glCodes:
                            glCodes[glCode]['amount'] += balance
            """

    def getGLSources(self):
        try:
            infile = os.path.join(inputFolder, "SCHEDULE11A1.xls")
            SCHEDULE11A1 = read_excel(infile, names=list('ABCDEFGHI'), converters={'B':str})
        except:
            self.showCriticalMessage("Cannot open %s file" % infile)
            sys.exit(1)

        try:
            infile = os.path.join(inputFolder, "SCHEDULE11G1.xls")
            SCHEDULE11G1 = read_excel(infile, names=list('ABCDEFGHIJKLMNOPQ'), converters={'B':str})
        except:
            self.showCriticalMessage("Cannot open %s file" % infile)
            sys.exit(1)

        try:
            infile = os.path.join(inputFolder, "SCHEDULE11G3.xls")
            SCHEDULE11G3 = read_excel(infile, names=list('ABCDEFGHIJKLMNO'), converters={'B':str})
        except:
            self.showCriticalMessage("Cannot open %s file" % infile)
            sys.exit(1)

        try:
            infile = os.path.join(inputFolder, "SCHEDULE29D1.xls")
            SCHEDULE29D1 = read_excel(infile, names=list('ABCDEFG'), converters={'B':str})
        except:
            self.showCriticalMessage("Cannot open %s file" % infile)
            sys.exit(1)

        try:
            infile = os.path.join(inputFolder, "SCHEDULE41.xls")
            SCHEDULE41 = read_excel(infile, names=list('ABCDEFGHIJKLMN'), converters={'B':str})
        except:
            self.showCriticalMessage("Cannot open %s file" % infile)
            sys.exit(1)

        cur.execute("Select gl_code, src_file, src_col from glcodes order by src_file, src_col") 
        for rec in cur.fetchall():
        #for idx, rec in enumerate(glSources):
            gl = rec[0]
            sourceFile = rec[1]
            sourceColumn = rec[2]
            amt = 0
            self.parent().updateStatusBar('Processing %s from %s' % (gl, sourceFile))

            if strip_right(sourceFile, '.xls') == 'SCHEDULE11A1':
                amt = SCHEDULE11A1[SCHEDULE11A1.B == gl][sourceColumn].values[0]
            elif strip_right(sourceFile, '.xls') == 'SCHEDULE11G1':
                amt = SCHEDULE11G1[SCHEDULE11G1.B == gl][sourceColumn].values[0]
            elif strip_right(sourceFile, '.xls') == 'SCHEDULE11G3':
                amt = SCHEDULE11G3[SCHEDULE11G3.B == gl][sourceColumn].values[0]
            elif strip_right(sourceFile, '.xls') == 'SCHEDULE29D1':
                amt = SCHEDULE29D1[SCHEDULE29D1.B == gl][sourceColumn].values[0]
            elif strip_right(sourceFile, '.xls') == 'SCHEDULE41':
                amt = SCHEDULE41[SCHEDULE41.B == gl][sourceColumn].values[0]

            #print "GL = %s, File = %s, Col = %s, Amt = %s" % (gl, sourceFile, sourceColumn, amt)
            amt = toFloat(amt)
            cur.execute("Update glcodes set amount = '{0}' where gl_code = '{1}' and src_file = '{2}' and src_col = '{3}'".format(amt, gl, sourceFile, sourceColumn))
            #glSources[idx]['amount'] = toFloat(amt)

        conn.commit()


    def writeFrpTemp(self):
        try:
            report = xl.Workbook(currentPath + "\\PPSB_Unified_Report.xlsm")
        except:
            self.showCriticalMessage("Cannot open 'PPSB_Unified_Report.xlsm' file")
            sys.exit(1)

        self.parent().updateStatusBar("Writing data to 'PPSB_Unified_Report.xlsm'")
        row = 2
        # get the worksheet object using COM api
        ws = report.xlWorkbook.ActiveSheet
        for gl in glCodes:
            for cnt in range(0, len(glCodes[gl]['dest']['wb'])):
                ws.Cells(row,1).Value = gl
                ws.Cells(row,4).Value = glCodes[gl]['amount']

                ws.Cells(row,2).Value = glCodes[gl]['dest']['wb'][cnt]
                ws.Cells(row,3).Value = glCodes[gl]['dest']['ws'][cnt] + '!' +\
                    glCodes[gl]['dest']['cell'][cnt]
                row += 1

        for rec in glSources:
            ws.Cells(row,1).Value = rec['gl_code']
            ws.Cells(row,2).Value = rec['dest']['wb']
            ws.Cells(row,3).Value = rec['dest']['ws'] + '!' + rec['dest']['cell']
            ws.Cells(row,4).Value = rec['amount']
            row += 1

        # blank out the remaining rows
        while ws.Cells(row, 1).Value not in [None, '']:
            row += 1

        # sort columns
        ws.Columns("A:D").Sort(Key1=ws.Range("B1"), Key2=ws.Range("C1"), Order1=1, Orientation=1, Header=1)

    def writeFrpFinal(self):
        global initDestination

        self.parent().updateStatusBar("Writing final data...")
        currWB = None
        currWS = None
        prvWB = None
        prvWS = None
        cur.execute("Select gl_code, dest_file, dest_sheet, dest_col, dest_cell, amount from glcodes order by dest_file, dest_sheet, dest_col, dest_cell")
        for rec in cur.fetchall():
            try:
                wb = os.path.join(outputFolder, rec[1].strip().strip('.xls').strip('.XLS'))
                ws = rec[2].strip()
                col = rec[3].strip()
                row = rec[4].strip()
                amt = rec[5]

                cell = col + row
                completeDest = wb + ws + cell
                self.parent().updateStatusBar("Writing data to [" + wb + "]" + ws + '!' + cell)
            except xl.range.ExcelRangeError:
                break

            if prvWB != wb:
                try:
                    currWB = xl.Workbook(wb + '.xls')
                except:
                    self.showCriticalMessage("Cannot open '%s.xls' file..." % wb)
                    self.exitProg(1)

                if prvWS != ws:
                    currWB.xlWorkbook.Worksheets(ws).Activate()
                    currWS = currWB.xlWorkbook.ActiveSheet
            elif prvWS != ws:
                currWB.xlWorkbook.Worksheets(ws).Activate()
                currWS = currWB.xlWorkbook.ActiveSheet

            try:
                # check if not yet in initDestination, then initialize destination amount to zero
                if completeDest not in initDestination:
                    currWS.Range(cell).Value = 0
                    initDestination.add(completeDest)

                currWS.Range(cell).Value += amt
            except:
                print "Cannot write on [%s]%s!%s" % (wb, ws, cell)

            prvWB = wb
            prvWS = ws

        self.parent().updateStatusBar("Process finished!")


    def getSettings(self):
        global mappingFile, inputFolder, outputFolder, settingWindowHeight, settingWindowWidth

        cur.execute("select setting, setting_value from ProgramSettings")
        for rec in cur.fetchall():
            if rec[0] == 'WindowHeight':
                if rec[1]:
                    settingWindowHeight = int(rec[1])
            elif rec[0] == 'WindowWidth':
                if rec[1]:
                    settingWindowWidth = int(rec[1])
            elif rec[0] == 'MappingFile':
                if rec[1]:
                    mappingFile = rec[1]
            elif rec[0] == 'InputFolder':
                if rec[1]:
                    inputFolder = rec[1]
            elif rec[0] == 'OutputFolder':
                if rec[1]:
                    outputFolder = rec[1]

    def saveSettings(self):
        # get main window size
        width = self.frameGeometry().width()
        height = self.frameGeometry().height()

        cur.execute("update ProgramSettings set Setting_value='%s' where Setting='WindowHeight'" % height)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='WindowWidth'" % width)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='MappingFile'" % mappingFile)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='InputFolder'" % inputFolder)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='OutputFolder'" % outputFolder)
        conn.commit()

    def exitProg(self, status):
        self.saveSettings()
        sys.exit(status)


def toFloat(strAmt):
    sign = 1
    validAmt = ''
    if strAmt and (type(strAmt) == unicode or type(strAmt) == str):
        for i in strAmt:
            if i in string.digits or i == '.':
                validAmt += i
            elif i == '(' or i == '-':
                sign = -1
        return float(validAmt) * sign
    elif type(strAmt) == float:
        return strAmt
    elif type(strAmt) == int:
        return float(strAmt)
    else:
        return float(0)

def strip_right(text, suffix):
    text = text.strip()
    result = text
    if text.endswith(suffix):
        result = text[:len(text)-len(suffix)]

    return result


def destSplit(dest):
    if not dest:
        return None, None, None

    if '[' not in dest or ']' not in dest or '!' not in dest:
        return None, None, None

    # get workbook name
    workbook = ''
    for pos in range(0, len(dest)):
        if dest[pos] == '[':
            continue
        elif dest[pos] == ']':
            break

        workbook += dest[pos]

    # get worksheet name
    worksheet = ''
    pos += 1
    for pos in range(pos, len(dest)):
        if dest[pos] == '!':
            break

        worksheet += dest[pos]

    # get cell name
    cellname = ''
    pos += 1
    for pos in range(pos, len(dest)):
        cellname += dest[pos]

    return workbook, worksheet, cellname

def openCreateDB():
    global conn, cur

    if os.path.exists('glcodes.db'):
        conn = sqlite3.connect('glcodes.db')
        cur = conn.cursor()
    else:
        conn = sqlite3.connect('glcodes.db')
        cur = conn.cursor()

        cur.execute('CREATE TABLE GLSOURCE (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL, SRC_FILE VARCHAR(200), SRC_SHEET VARCHAR(100),\
                SRC_COL VARCHAR(25), SRC_CELL VARCHAR(25), PRIMARY KEY(ID))')
        cur.execute('CREATE UNIQUE INDEX idx_glsource ON GLSOURCE(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')
        #cur.execute('CREATE UNIQUE INDEX idx_glsource2 ON GLSOURCE(SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')

        cur.execute('CREATE TABLE GLCODES (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL,\
        	SRC_FILE VARCHAR(200), SRC_SHEET VARCHAR(200), SRC_COL VARCHAR(25), SRC_CELL VARCHAR(25),\
        	DEST_FILE VARCHAR(200), DEST_SHEET VARCHAR(100), DEST_COL VARCHAR(25), DEST_CELL VARCHAR(25), AMOUNT REAL,\
        	PRIMARY KEY (ID))')
        cur.execute('CREATE INDEX idx_glsrc ON GLCODES(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')
        cur.execute('CREATE INDEX idx_gldest ON GLCODES(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)')
        

        cur.execute('CREATE TABLE GLDEST (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL, DEST_FILE VARCHAR(200),\
                DEST_SHEET VARCHAR(100), DEST_COL VARCHAR(25), DEST_CELL VARCHAR(25), PRIMARY KEY (ID))')
        cur.execute('CREATE UNIQUE INDEX idx_gldest ON GLDEST(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)')

        #cur.execute('CREATE TABLE GLCODES (GL_CODE VARCHAR(25) NOT NULL, AMOUNT REAL DEFAULT 0,PRIMARY KEY (GL_CODE))')
        #cur.execute('CREATE UNIQUE INDEX idx_glcodes ON GLCODES (GL_CODE)')

        cur.execute('CREATE VIEW GLSOURCE_VW AS select gl_code, src_file, src_sheet, src_col, src_cell from glsource order by src_file, src_sheet, src_col, src_cell')
        cur.execute('CREATE VIEW GLDEST_VW AS select gl_code, dest_file, dest_sheet, dest_col, dest_cell from GLDEST order by dest_file, dest_sheet, dest_col, dest_cell')

        cur.execute('CREATE TABLE ProgramSettings (ID INTEGER NOT NULL, SETTING VARCHAR(50) NOT NULL, SETTING_VALUE VARCHAR(250), PRIMARY KEY (ID), UNIQUE (SETTING))')


glCodes = None
initDestination = set()
mappingFile = None
inputFolder = None
outputFolder = None
settingWindowHeight = None
settingWindowWidth = None
currentPath = os.getcwd()
openCreateDB()
app = QApplication(sys.argv)
form = FRP_MainWindow()
form.show()
sys.exit(app.exec_())
'''
if __name__ == '__main__':
    readFrpMapping()
    getDailyTrialBalance()
    getGLSources()
    writeFrpTemp()
    writeFrpFinal()
'''
