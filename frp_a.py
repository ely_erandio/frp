#import xl
import xlrd
import xlwings as xw
import string
from collections import defaultdict
import os
import sys
import math
from pandas import read_excel, isnull
import numpy as np
from PySide.QtCore import *
from PySide.QtGui import *
import sqlite3
import csv

class FRP_MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setGeometry(400, 100, 270, 100)
        self.setWindowTitle('FRP Report Generator')

        self.centralWidget = FRP_widget()
        self.setCentralWidget(self.centralWidget)

        # background image for QMainWindow
        # palette = QPalette()
        # palette.setBrush(QPalette.Background, QBrush(QPixmap('ppsb_logo.jpg').scaled(self.size())))
        # self.setPalette(palette)

        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
        self.statusBar.showMessage('Ready')
        if settingWindowHeight and settingWindowWidth:
            self.resize(settingWindowWidth, settingWindowHeight)

    def updateStatusBar(self, message):
        self.statusBar.showMessage(message)


class FRP_widget(QWidget):
    def __init__(self, parent=None):
        super(FRP_widget, self).__init__(parent)
        self.setWindowTitle('FRP Report Generator')
        self.getSettings()
        # self.resize(320, 200)

        # mapping file
        gridLayout = QGridLayout()
        lblMap = QLabel("Mapping File")
        self.mappingFile = QLineEdit(); self.mappingFile.setReadOnly(True)
        self.btnMappingFile = QPushButton('...')

        lblInputFolder = QLabel("Input Folder")
        self.inputFolder = QLineEdit(); self.inputFolder.setReadOnly(True)
        self.btnInputFolder = QPushButton('...')

        lblOutputFolder = QLabel("Output Folder")
        self.outputFolder = QLineEdit(); self.outputFolder.setReadOnly(True)
        self.btnOutputFolder = QPushButton('...')

        lblDepoSizing = QLabel("Deposit Report File")
        self.depoSizing = QLineEdit(); self.depoSizing.setReadOnly(True)
        self.btnDepoSizing = QPushButton('...')

        gridLayout.addWidget(lblMap, 0, 0)
        gridLayout.addWidget(self.mappingFile, 0, 1)
        gridLayout.addWidget(self.btnMappingFile, 0, 2)

        gridLayout.addWidget(lblInputFolder, 1, 0)
        gridLayout.addWidget(self.inputFolder, 1, 1)
        gridLayout.addWidget(self.btnInputFolder, 1, 2)

        gridLayout.addWidget(lblOutputFolder, 2, 0)
        gridLayout.addWidget(self.outputFolder, 2, 1)
        gridLayout.addWidget(self.btnOutputFolder, 2, 2)

        gridLayout.addWidget(lblDepoSizing, 3, 0)
        gridLayout.addWidget(self.depoSizing, 3, 1)
        gridLayout.addWidget(self.btnDepoSizing, 3, 2)
        '''
        # check box
        checkLayout = QVBoxLayout()
        self.chkGenerateData = QCheckBox('Generate Data', self)
        self.chkGenerateData.setCheckState(Qt.Checked)
        self.chkGenerateReport = QCheckBox('Generate Report', self)
        self.chkGenerateReport.setCheckState(Qt.Checked)
        checkLayout.addWidget(self.chkGenerateData)
        checkLayout.addWidget(self.chkGenerateReport)
        '''

        self.btnCancel = QPushButton('Cancel')
        self.btnExecute = QPushButton('Execute')
        btnLayout = QHBoxLayout()
        btnLayout.addWidget(self.btnCancel)
        btnLayout.addWidget(self.btnExecute)

        layout = QVBoxLayout()
        layout.addLayout(gridLayout)
        #layout.addLayout(checkLayout)
        layout.addLayout(btnLayout)
        self.setLayout(layout)

        self.btnMappingFile.clicked.connect(self.onBtnMappingFileClicked)
        self.btnInputFolder.clicked.connect(self.onBtnInputFolderClicked)
        self.btnOutputFolder.clicked.connect(self.onBtnOutputFolderClicked)
        self.btnDepoSizing.clicked.connect(self.onBtnDepoSizingClicked)
        #self.chkGenerateData.stateChanged.connect(self.updateUi)
        #self.chkGenerateReport.stateChanged.connect(self.updateUi)
        self.btnCancel.clicked.connect(self.exit_)
        self.btnExecute.clicked.connect(self.process)
        
        self.initUI()

    def initUI(self):
        if mappingFile:
            self.mappingFile.setText(mappingFile)
        if inputFolder:
            self.inputFolder.setText(inputFolder)
        if outputFolder:
            self.outputFolder.setText(outputFolder)

    def onBtnMappingFileClicked(self):
        global mappingFile

        filename, filter = QFileDialog.getOpenFileName(parent=self, caption='Get Mapping File', dir='.', filter='*.xls')

        if filename:
            self.mappingFile.setText(filename)
            mappingFile = filename

    def onBtnInputFolderClicked(self):
        global inputFolder

        directory = QFileDialog.getExistingDirectory(self, caption='Select Input Directory', options=QFileDialog.ShowDirsOnly)

        if directory:
            self.inputFolder.setText(directory)
            inputFolder = directory

    def onBtnOutputFolderClicked(self):
        global outputFolder

        directory = QFileDialog.getExistingDirectory(self, caption='Select Output Directory', options=QFileDialog.ShowDirsOnly)

        if directory:
            self.outputFolder.setText(directory)
            outputFolder = directory

    def onBtnDepoSizingClicked(self):
        global depositSizingFile

        filename, filter = QFileDialog.getOpenFileName(self, caption='Get Deposit Report File', dir='.', filter='*.csv')
        if filename:
            self.depoSizing.setText(filename)
            depositSizingFile = filename

    def process(self):
        # get data if chkGenerateData is checked
        self.truncateTables()
        self.readFrpMapping()
        self.getGLSources()
        self.getDepositSizing()
        #self.writeFrpTemp()

        self.writeFrpFinal()
        self.btnCancel.setText('Exit')
        self.btnExecute.setEnabled(False)
        QMessageBox.information(self, "FRP Report Generation", "The process has been completed.")

    def updateUi(self):
        if not self.chkGenerateData.isChecked() and not self.chkGenerateReport.isChecked():
            self.btnExecute.setEnabled(False)
        else:
            self.btnExecute.setEnabled(True)

    def exit_(self):
        if self.btnCancel.text() == 'Cancel':
            # show the confirmation message
            flags = QMessageBox.StandardButton.Yes
            flags |= QMessageBox.StandardButton.No
            question = "Do you really want to cancel?"
            response = QMessageBox.question(self, "Confirm Cancel", question, flags, QMessageBox.No)
            if response == QMessageBox.No:
                return

        self.exitProg(0)

    def showCriticalMessage(self, msg):
        flags = QMessageBox.StandardButton.Ok
        QMessageBox.critical(self, "CRITICAL ERROR", msg, flags)

    def truncateTables(self):
    	#cur.execute('DELETE FROM GLSOURCE')
    	#cur.execute('DELETE FROM GLDEST')
    	cur.execute('DELETE FROM GLCODES')
    	conn.commit()

    def isNan(self, str):
        return str != str

    def readFrpMapping(self):
        global glCodes, glSources

        try:
            # wb = xl.Workbook(currentPath + '\\FRP Mapping.xlsx')
            converters = {column: str for column in range(10)}
            df = read_excel(mappingFile, converters=converters)
        except:
            self.showCriticalMessage("Cannot open %s file" % mappingFile)
            self.exitProg(1)

        glCodes = defaultdict(dict)
        glSources = []

        self.parent().updateStatusBar('Reading %s' % mappingFile)
        cnt = 0
        for row in df.itertuples():
            cnt = cnt + 1
            self.parent().updateStatusBar('Reading %s record # %d' % (mappingFile, cnt))
            gl = row[1]
            gl = gl.strip()
            src_file = row[2]
            src_sheet = row[3]
            src_col = str(row[4]).strip()
            src_cell = str(row[5]).strip()
            gl_col = str(row[6]).strip()
            dest_file = row[7]
            dest_sheet = row[8]
            dest_col = str(row[9]).strip()
            dest_cell = str(row[10]).strip()

            # replace nan values with blank
            if src_sheet == 'nan':
                src_sheet = ''
            if src_col == 'nan':
                src_col = ''
            if src_cell == 'nan':
                src_cell = ''
            if gl_col == 'nan':
                gl_col = ''

            # continue if gl is blank
            if not gl or gl == 'nan':
                continue

            # save in glcodes table
            cur.execute("SELECT * FROM GLCODES WHERE GL_CODE = '{0}' and SRC_FILE = '{1}' and SRC_SHEET = '{2}' and SRC_COL = '{3}' and SRC_CELL = '{4}'\
                        and GL_COL = '{5}' and DEST_FILE = '{6}' and DEST_SHEET = '{7}' and DEST_COL = '{8}' and DEST_CELL = '{9}'".\
                        format(gl, src_file, src_sheet, src_col, src_cell, gl_col, dest_file, dest_sheet, dest_col, dest_cell))
            rec = cur.fetchall()

            # if record does not exists
            if not rec:
                sql_stmt = "INSERT INTO GLCODES(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL, GL_COL, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)"\
                            " VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')".format(gl, src_file, src_sheet, src_col, src_cell, gl_col, 
                                    dest_file, dest_sheet, dest_col, dest_cell)
                #print sql_stmt
                cur.execute(sql_stmt)
                #conn.commit()


            '''
            # save in gldest table
            sql_stmt = "SELECT * FROM GLDEST WHERE GL_CODE = '{0}' and DEST_FILE = '{1}' and DEST_SHEET = '{2}' and DEST_COL = '{3}' and DEST_CELL = '{4}'".\
                        format(gl, dest_file, dest_sheet, dest_col, dest_cell)
            cur.execute(sql_stmt)
            rec = cur.fetchall()

            # if record does not exists
            if not rec:
                sql_stmt = "INSERT INTO GLDEST(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)"\
                            " VALUES('{0}', '{1}', '{2}', '{3}', '{4}')".format(gl, dest_file, dest_sheet, dest_col, dest_cell)
                cur.execute(sql_stmt)
                #conn.commit()

			
			# save in glcodes table (w/ amount)                
            cur.execute("SELECT * FROM GLCODES WHERE GL_CODE = '{0}'".format(gl))
            rec = cur.fetchall()
            if not rec:
            	cur.execute("INSERT INTO GLCODES(GL_CODE) VALUES('{0}')".format(gl))
                #conn.commit()
        ''' 
        conn.commit()

    def getGLSources(self):

        cur.execute("Select gl_code, src_file, src_sheet, src_col, src_cell, gl_col from glcodes order by src_file, src_sheet, src_col, src_cell") 
        prevFile = ''
        prevSheet = ''
        for rec in cur.fetchall():
        #for idx, rec in enumerate(glSources):
            gl = rec[0]
            sourceFile = rec[1].strip()
            fullpath = os.path.join(inputFolder, sourceFile)
            sourceSheet = rec[2]
            sourceColumn = rec[3]
            sourceCell = rec[4]
            glColumn = rec[5]
            amt = 0
            cell = "%s%s" % (sourceColumn, sourceCell)
            if not sourceFile or isnull(sourceFile) or self.isNan(sourceFile) or sourceFile == 'nan':
                continue
            if not sourceSheet or isnull(sourceSheet) or self.isNan(sourceSheet):
                sourceSheet = ''

            self.parent().updateStatusBar('Processing %s from %s' % (gl, sourceFile))

            if prevFile != sourceFile:
                try:
                    currWB = xw.Book(fullpath)
                except:
                    self.showCriticalMessage("Cannot open '%s' file..." % fullpath)
                    self.exitProg(1)

                glCodes = {}
                try:
                    currWS = currWB.sheets[sourceSheet]
                except:
                    self.showCriticalMessage("Cannot open worksheet '%s' in '%s' file..." % (sourceSheet, sourceFile))
                    self.exitProg(1)
                currRow = 1

            elif prevSheet != sourceSheet and sourceSheet != '':
                try:
                    currWS = currWB.sheets[sourceSheet]
                except:
                    self.showCriticalMessage("Cannot open worksheet '%s' in '%s' file..." % (sourceSheet, sourceFile))
                    self.exitProg(1)
                currRow = 1

            if (sourceCell == '' or sourceSheet == 'isnan') and glColumn != '' and glColumn != 'isnan':
                while True:
                    cellGL = "%s%s" % (glColumn, currRow)
                    cellAmt = "%s%s" % (sourceColumn, currRow)
                    #glInput = str(currWS.range(cellGL).value)
                    glInput = str(currWS.range(cellGL).value)
                    amtInput = currWS.range(cellAmt).value

                    # replace '<>' with '()'
                    if isinstance(amtInput, str) or isinstance(amtInput, unicode):
                        amtInput = amtInput.replace('<', '(')
                        amtInput = amtInput.replace('>', ')')

                    # change glInput to string
                    glInput = glInput.rstrip('0').rstrip('.')
                    glCodes[glInput] = amtInput
                    if currWS.range(cellGL).value is None:
                        break
                    if glInput is None or glInput == '' or not glInput:
                        break
                    
                    #print "cellGL=[%s][%s]\tcellAmt=[%s][%s]\n" % (cellGL, glInput, cellAmt, amtInput)
                
                    currRow += 1
            
            

            if glColumn != '' and glColumn != 'isnan':
                if gl not in glCodes:
                    self.parent().updateStatusBar('%s not in %s' % (gl, sourceFile))
                    amt = 0
                    print "GL %s not in %s" % (gl, sourceFile)
                else:
                    amt = glCodes[gl]
            else:
                amt = currWS.range(cell).value

            #print "GL = %s, File = %s, Col = %s, Amt = %s" % (gl, sourceFile, sourceColumn, amt)
            cur.execute("Update glcodes set amount = '{0}' where gl_code='{1}' and src_file='{2}' and src_sheet='{3}' and src_col='{4}' and src_cell='{5}'".format(amt, gl, sourceFile, sourceSheet, sourceColumn, sourceCell))
            #glSources[idx]['amount'] = toFloat(amt)

            prevFile = sourceFile
            prevSheet = sourceSheet

        conn.commit()


    def getDepositSizing(self):
        global depositSizings

        # if there is no Deposit Report File, return immediately
        if not depositSizingFile or depositSizingFile.strip() == '':
            return
        try:
            input = csv.reader(open(depositSizingFile))
            input.next()                                    # throw the header line
        except:
            self.showCriticalMessage("Cannot open %s file" % depositSizingFile)
            self.exitProg(1)

        depositSizings = {}
        depositSizings = { 5000: {}, 10000: {}, 15000: {}, 20000:{}, 30000:{}, 40000: {}, 50000:{}, 60000: {}, 80000: {}, 100000: {},\
            150000: {}, 200000: {}, 250000: {}, 300000: {}, 400000:{}, 500000:{}, 750000:{}, 1000000:{}, 1500000:{}, 2000000:{},\
            3000000:{}, 4000000:{}, 5000000:{}, 999999999: {}}

        prevAmt = 0 - 0.01
        for i in sorted(depositSizings.keys()):
            description = "P {:,.2f} - {:,.2f}".format(prevAmt+0.01,i)
            if prevAmt < 0:
                description = "P {:,.2f} & Below".format(i)
            elif i > 5000000:
                description = "Over P 5,000,000.00"
            
            depositSizings[i]['description'] = description
            depositSizings[i]['min'] = prevAmt + 0.01
            depositSizings[i]['max'] = i
            depositSizings[i]['count'] = 0
            depositSizings[i]['totalAmount'] = 0
        
            prevAmt = i
            
        totalCount = 0
        for rec in input:
            productID = rec[0]
            accountNum = rec[2]
            balance = float(rec[8])
                
            for i in sorted(depositSizings.keys()):
                if balance >= depositSizings[i]['min'] and balance <= depositSizings[i]['max']:
                    depositSizings[i]['count'] += 1
                    depositSizings[i]['totalAmount'] += balance
                    break
            
            totalCount += 1
            

    def writeFrpFinal(self):
        global initDestination

        self.parent().updateStatusBar("Writing final data...")
        currWB = None
        currWS = None
        prvWB = None
        prvWS = None
        cur.execute("Select gl_code, dest_file, dest_sheet, dest_col, dest_cell, amount from glcodes order by dest_file, dest_sheet, dest_col, dest_cell")
        for rec in cur.fetchall():
            try:
                wb = os.path.join(outputFolder, rec[1].strip())
                ws = rec[2].strip()
                col = rec[3].strip()
                row = rec[4].strip()
                amt = rec[5]

                if not rec[1].strip() or isnull(rec[1]) or rec[1].strip() == 'nan':
                    continue

                cell = "%s%s" % (col, row)
                completeDest = wb + ws + cell
                self.parent().updateStatusBar("Writing data to [" + wb + "]" + ws + '!' + cell)
            except xl.range.ExcelRangeError:
                break

            if prvWB != wb:
                try:
                    currWB = xw.Book(wb)
                except:
                    self.showCriticalMessage("Cannot open '%s' file..." % wb)
                    self.exitProg(1)

                try:
                    currWS = currWB.sheets[ws]
                except:
                    self.showCriticalMessage("Cannot open worksheet '%s' in '%s' file..." % (ws,wb))
                    self.exitProg(1)

            elif prvWS != ws:
                try:
                    currWS = currWB.sheets[ws]
                except:
                    self.showCriticalMessage("Cannot open worksheet '%s' in '%s' file..." % (ws,wb))
                    self.exitProg(1)

            try:
                # check if not yet in initDestination, then initialize destination amount to zero
                if completeDest not in initDestination:
                    initDestination.add(completeDest)
                    currWS.range(cell).value = 0

                currWS.range(cell).value += amt
            except:
                self.showCriticalMessage("Cannot write on [%s]%s!%s" % (wb,ws,cell))

            prvWB = wb
            prvWS = ws

        self.writeDepositSizing()
        self.parent().updateStatusBar("Process finished!")


    def writeDepositSizing(self):
        wb = os.path.join(outputFolder, 'RB-SOLO-DEPLIAB.xlsm')
        try:
            currWB = xw.Book(wb)
        except:
            self.showCriticalMessage("Cannot open '%s' file..." % wb)
            #self.exitProg(1)
            return

        try:
            currWS = currWB.sheets['22A']
        except:
            self.showCriticalMessage("Cannot open worksheet '22A' in '%s' file..." % wb)
            self.exitProg(1)

        for size in sorted(depositSizings.keys()):
            if size == 5000:
                currWS.range('Q11').value = depositSizings[size]['count']
                currWS.range('R11').value = depositSizings[size]['totalAmount']
            elif size == 10000:
                currWS.range('Q12').value = depositSizings[size]['count']
                currWS.range('R12').value = depositSizings[size]['totalAmount']
            elif size == 15000:
                currWS.range('Q13').value = depositSizings[size]['count']
                currWS.range('R13').value = depositSizings[size]['totalAmount']
            elif size == 20000:
                currWS.range('Q14').value = depositSizings[size]['count']
                currWS.range('R14').value = depositSizings[size]['totalAmount']
            elif size == 30000:
                currWS.range('Q15').value = depositSizings[size]['count']
                currWS.range('R15').value = depositSizings[size]['totalAmount']
            elif size == 40000:
                currWS.range('Q16').value = depositSizings[size]['count']
                currWS.range('R16').value = depositSizings[size]['totalAmount']
            elif size == 50000:
                currWS.range('Q17').value = depositSizings[size]['count']
                currWS.range('R17').value = depositSizings[size]['totalAmount']
            elif size == 60000:
                currWS.range('Q18').value = depositSizings[size]['count']
                currWS.range('R18').value = depositSizings[size]['totalAmount']
            elif size == 80000:
                currWS.range('Q19').value = depositSizings[size]['count']
                currWS.range('R19').value = depositSizings[size]['totalAmount']
            elif size == 100000:
                currWS.range('Q20').value = depositSizings[size]['count']
                currWS.range('R20').value = depositSizings[size]['totalAmount']
            elif size == 150000:
                currWS.range('Q21').value = depositSizings[size]['count']
                currWS.range('R21').value = depositSizings[size]['totalAmount']
            elif size == 200000:
                currWS.range('Q22').value = depositSizings[size]['count']
                currWS.range('R22').value = depositSizings[size]['totalAmount']
            elif size == 250000:
                currWS.range('Q23').value = depositSizings[size]['count']
                currWS.range('R23').value = depositSizings[size]['totalAmount']
            elif size == 300000:
                currWS.range('Q24').value = depositSizings[size]['count']
                currWS.range('R24').value = depositSizings[size]['totalAmount']
            elif size == 400000:
                currWS.range('Q25').value = depositSizings[size]['count']
                currWS.range('R25').value = depositSizings[size]['totalAmount']
            elif size == 500000:
                currWS.range('Q26').value = depositSizings[size]['count']
                currWS.range('R26').value = depositSizings[size]['totalAmount']
            elif size == 750000:
                currWS.range('Q27').value = depositSizings[size]['count']
                currWS.range('R27').value = depositSizings[size]['totalAmount']
            elif size == 1000000:
                currWS.range('Q28').value = depositSizings[size]['count']
                currWS.range('R28').value = depositSizings[size]['totalAmount']
            elif size == 1500000:
                currWS.range('Q29').value = depositSizings[size]['count']
                currWS.range('R29').value = depositSizings[size]['totalAmount']
            elif size == 2000000:
                currWS.range('Q30').value = depositSizings[size]['count']
                currWS.range('R30').value = depositSizings[size]['totalAmount']
            elif size == 3000000:
                currWS.range('Q31').value = depositSizings[size]['count']
                currWS.range('R31').value = depositSizings[size]['totalAmount']
            elif size == 4000000:
                currWS.range('Q32').value = depositSizings[size]['count']
                currWS.range('R32').value = depositSizings[size]['totalAmount']
            elif size == 5000000:
                currWS.range('Q33').value = depositSizings[size]['count']
                currWS.range('R33').value = depositSizings[size]['totalAmount']
            elif size > 5000000:
                currWS.range('Q34').value = depositSizings[size]['count']
                currWS.range('R34').value = depositSizings[size]['totalAmount']
        
    def getSettings(self):
        global mappingFile, inputFolder, outputFolder, settingWindowHeight, settingWindowWidth

        cur.execute("select setting, setting_value from ProgramSettings")
        for rec in cur.fetchall():
            if rec[0] == 'WindowHeight':
                if rec[1]:
                    settingWindowHeight = int(rec[1])
            elif rec[0] == 'WindowWidth':
                if rec[1]:
                    settingWindowWidth = int(rec[1])
            elif rec[0] == 'MappingFile':
                if rec[1]:
                    mappingFile = rec[1]
            elif rec[0] == 'InputFolder':
                if rec[1]:
                    inputFolder = rec[1]
            elif rec[0] == 'OutputFolder':
                if rec[1]:
                    outputFolder = rec[1]

    def saveSettings(self):
        # get main window size
        width = self.frameGeometry().width()
        height = self.frameGeometry().height()

        cur.execute("update ProgramSettings set Setting_value='%s' where Setting='WindowHeight'" % height)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='WindowWidth'" % width)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='MappingFile'" % mappingFile)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='InputFolder'" % inputFolder)
        cur.execute("update ProgramSettings set Setting_Value='%s' where Setting='OutputFolder'" % outputFolder)
        conn.commit()

    def exitProg(self, status):
        self.saveSettings()
        sys.exit(status)


def toFloat(strAmt):
    sign = 1
    validAmt = ''
    if strAmt and (type(strAmt) == unicode or type(strAmt) == str):
        for i in strAmt:
            if i in string.digits or i == '.':
                validAmt += i
            elif i == '(' or i == '-':
                sign = -1
        return float(validAmt) * sign
    elif type(strAmt) == float:
        return strAmt
    elif type(strAmt) == int:
        return float(strAmt)
    else:
        return float(0)

def strip_right(text, suffix):
    text = text.strip()
    result = text
    if text.endswith(suffix):
        result = text[:len(text)-len(suffix)]

    return result


def destSplit(dest):
    if not dest:
        return None, None, None

    if '[' not in dest or ']' not in dest or '!' not in dest:
        return None, None, None

    # get workbook name
    workbook = ''
    for pos in range(0, len(dest)):
        if dest[pos] == '[':
            continue
        elif dest[pos] == ']':
            break

        workbook += dest[pos]

    # get worksheet name
    worksheet = ''
    pos += 1
    for pos in range(pos, len(dest)):
        if dest[pos] == '!':
            break

        worksheet += dest[pos]

    # get cell name
    cellname = ''
    pos += 1
    for pos in range(pos, len(dest)):
        cellname += dest[pos]

    return workbook, worksheet, cellname

def openCreateDB():
    global conn, cur

    if os.path.exists('glcodes.db'):
        conn = sqlite3.connect('glcodes.db')
        cur = conn.cursor()
    else:
        conn = sqlite3.connect('glcodes.db')
        cur = conn.cursor()

        #cur.execute('CREATE TABLE GLSOURCE (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL, SRC_FILE VARCHAR(200), SRC_SHEET VARCHAR(100),\
        #        SRC_COL VARCHAR(25), SRC_CELL VARCHAR(25), PRIMARY KEY(ID))')
        #cur.execute('CREATE UNIQUE INDEX idx_glsource ON GLSOURCE(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')
        #cur.execute('CREATE UNIQUE INDEX idx_glsource2 ON GLSOURCE(SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')

        cur.execute('CREATE TABLE GLCODES (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL,\
        	SRC_FILE VARCHAR(200), SRC_SHEET VARCHAR(200), SRC_COL VARCHAR(25), SRC_CELL VARCHAR(25), GL_COL VARCHAR(25),\
        	DEST_FILE VARCHAR(200), DEST_SHEET VARCHAR(100), DEST_COL VARCHAR(25), DEST_CELL VARCHAR(25), AMOUNT REAL,\
        	PRIMARY KEY (ID))')
        cur.execute('CREATE INDEX idx_glsrc ON GLCODES(GL_CODE, SRC_FILE, SRC_SHEET, SRC_COL, SRC_CELL)')
        cur.execute('CREATE INDEX idx_gldest ON GLCODES(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)')
        

        #cur.execute('CREATE TABLE GLDEST (ID INTEGER NOT NULL, GL_CODE VARCHAR(25) NOT NULL, DEST_FILE VARCHAR(200),\
        #        DEST_SHEET VARCHAR(100), DEST_COL VARCHAR(25), DEST_CELL VARCHAR(25), PRIMARY KEY (ID))')
        #cur.execute('CREATE UNIQUE INDEX idx_gldest2 ON GLDEST(GL_CODE, DEST_FILE, DEST_SHEET, DEST_COL, DEST_CELL)')

        #cur.execute('CREATE TABLE GLCODES (GL_CODE VARCHAR(25) NOT NULL, AMOUNT REAL DEFAULT 0,PRIMARY KEY (GL_CODE))')
        #cur.execute('CREATE UNIQUE INDEX idx_glcodes ON GLCODES (GL_CODE)')

        #cur.execute('CREATE VIEW GLSOURCE_VW AS select gl_code, src_file, src_sheet, src_col, src_cell from glsource order by src_file, src_sheet, src_col, src_cell')
        #cur.execute('CREATE VIEW GLDEST_VW AS select gl_code, dest_file, dest_sheet, dest_col, dest_cell from GLDEST order by dest_file, dest_sheet, dest_col, dest_cell')

        cur.execute('CREATE TABLE ProgramSettings (ID INTEGER NOT NULL, SETTING VARCHAR(50) NOT NULL, SETTING_VALUE VARCHAR(250), PRIMARY KEY (ID), UNIQUE (SETTING))')
        cur.execute("insert into ProgramSettings(setting, setting_value) values('WindowHeight', 0)")
        cur.execute("insert into ProgramSettings(setting, setting_value) values('WindowWidth', 0)")
        cur.execute("insert into ProgramSettings(setting, setting_value) values('MappingFile', '')")
        cur.execute("insert into ProgramSettings(setting, setting_value) values('InputFolder', '')")
        cur.execute("insert into ProgramSettings(setting, setting_value) values('OutputFolder', '')")


glCodes = None
initDestination = set()
mappingFile = None
inputFolder = None
outputFolder = None
settingWindowHeight = None
settingWindowWidth = None
depositSizingFile = ''
depositSizings = {}
currentPath = os.getcwd()
openCreateDB()
app = QApplication(sys.argv)
form = FRP_MainWindow()
form.show()
sys.exit(app.exec_())
'''
if __name__ == '__main__':
    readFrpMapping()
    getGLSources()
    writeFrpTemp()
    writeFrpFinal()
'''
